from talon.voice import Context, Key

ides = ['com.microsoft.VSCode']
ctx = Context('vscode', func=lambda app, win: any(
    i in app.bundle for i in ides))

keymap = {
    #navigation
    'go file': Key('cmd-p'),
    'go line': Key('ctrl-g'),
    'jumpy': Key('ctrl+-'),
    'jumper': Key('ctrl-shift+-'),
    'zin mode': Key('cmd-k z'),
    'show side': Key('cmd-b'),
    'terminal': Key('ctrl-`'),
    'close others': Key('alt-cmd-t'),
    'keep open': [Key('cmd-k'), Key('enter')],
    'add window': Key('shift-cmd-n'),
    'switcher': Key('ctrl-w'),
    'celsa': Key('ctrl-alt-cmd-e'),
    'celk': Key('ctrl-alt-cmd-r'),


    #debug
    'debug': Key('shift-cmd-d'),
    '(start bug | runnit)': Key('f5'),
    'stop bug': Key('shift-f5'),
    'stepper': Key('f10'),
    'steppin': Key('f11'),
    'stepout': Key('shift-f11'),
    'bapoint': Key('f9'),

    #editing
    # 'comment declaration': ['/**', Key('space')],
    'comment block': ['/**', Key('enter')],
    
    'autoformat': Key('shift-alt-f'),
    'comment': Key('cmd-/'),
    'import class': Key('alt-enter enter'),
    'doubler': Key('cmd-i cmd-c cmd-v cmd-v'),
    'destroyer': Key('cmd-i delete delete'),
    
    #selection
    'bracken': Key('cmd-shift-ctrl-right'),
    'get next': Key('cmd-d'),
    'skip next': Key('cmd-k cmd-d'),
    'select line': Key('cmd-i'),
    'copy line': Key('cmd-i cmd-c'),
    
    # Words
    'to do': 'TODO',

}

ctx.keymap(keymap)


