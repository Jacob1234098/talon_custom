from talon.voice import Context, Key

languages = ['.js', '.py', '.css', '.html', '.less', '.sql']
bundles = ['com.microsoft.VSCode']

ctx = Context('code', func=lambda app, win:
              any(app.bundle == b for b in bundles)
              or any(win.doc.endswith(l) for l in languages)
              )

keymap = {
    'sinker': [Key('cmd-right ;')],
 
    'args': ['()', Key('left')],
    'angler': ['<>', Key('left')],
    'quoticks': ['``', Key('left')],
    '(block | kirblock)': ['{}', Key('left enter')],
    'args-block': ['(', Key('enter')],
    'brax-block': ['[', Key('enter')],
    '(index | brax)': ['[]', Key('left')],

    'coalshock': [':', Key('enter')],
    'coal twice': '::',
    'ellipsis': '...',
    'mintwice': '--',
    'plustwice': '++',
    '(dub pipe | dub spike)': '||',
    '(dubamp | dub amp)': '&&',

    '(indirect | reference)': '&',
    '([is] equal to | longqual)': ' == ',
    '([is] not equal to | banquall)': ' != ',
    'trickle': ' === ',
    '(ranqual | nockle)': ' !== ',
    '(call | prekris)': '()',

    '(empty dict | kirk)': ['{}', Key('left')],

    '(empty array | brackers)': '[]',
    'empty dict': '{}',
    'minquall': '-=',
    'pluqual': '+=',
    'starqual': '*=',
    'lessqual': ' <= ',
    'grayqual': ' >= ',

    '(arrow | lambo)': '->',
    'shrocket': ' => ',

    'state if': ['if ()', Key('left')],
    'state else': ['else {}', Key('left enter')],
    'state else if': ['else if ()', Key('left')],
    'state while': ['while ()', Key('left')],
    'state for': ['for ()', Key('left')],
    'state for each': ['foreach ()', Key('left')],
    'state switch': ['switch ()', Key('left')],

    'cons': 'const ',
    'static': 'static ',
    'tip pent': 'int ',
    'letta': 'let ',

    'word no': 'null',
    'word big no': 'NULL',
    'word printf': 'printf',
    'word define': 'def ',
    'word import': 'import ',
    'word params': 'params ',
}

ctx.keymap(keymap)


