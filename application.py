from talon.voice import Context, Key

ctx = Context('application')

keymap = {
    'prefies': Key('cmd-,'),
    'marco': Key('cmd-f'),
    'marcall': Key('cmd-shift-f'),
    'full screen': Key('cmd-ctrl-f')
}

ctx.keymap(keymap)


