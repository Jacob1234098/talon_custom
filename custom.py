from talon import tap, applescript

def on_key(typ, e):
    if e == 'cmd-alt-ctrl-shift-t' and e.down:
        print('mute')
        applescript.run('set volume input volume 0')
    if e == 'cmd-alt-ctrl-shift-y' and e.down:
        print('unmute')
        applescript.run('set volume input volume 100')
tap.register(tap.HOOK | tap.KEY, on_key)