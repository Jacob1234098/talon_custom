from talon.voice import Context, Key

ides = ['com.torusknot.SourceTreeNotMAS']
ctx = Context('sourcetree', func=lambda app, win: any(
    i in app.bundle for i in ides))

keymap = {
    #navigation
    'switcher': Key('cmd-0'),
    
}

ctx.keymap(keymap)


