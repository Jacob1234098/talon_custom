from talon.voice import Word, Context, Key, Rep, Str, press
from talon import ctrl
import string

alpha_alt = 'air bat cap die each fail gone harm sit jury crash look mad near odd pit quest red sun trap urge vest whale box yes zip'.split()
alnum = list(zip(alpha_alt, string.ascii_lowercase)) + [(str(i), str(i)) for i in range(0, 10)]

alpha = {}
alpha.update(dict(alnum))
alpha.update({'ship %s' % word: letter for word, letter in zip(alpha_alt, string.ascii_uppercase)})

alpha.update({'control %s' % k: Key('ctrl-%s' % v) for k, v in alnum})
alpha.update({'command %s' % k: Key('cmd-%s' % v) for k, v in alnum})
alpha.update({'command shift %s' % k: Key('ctrl-shift-%s' % v) for k, v in alnum})
alpha.update({'alt %s' % k: Key('alt-%s' % v) for k, v in alnum})

mapping = {
    'semicolon': ';',
    r'new-line': '\n',
    r'new-paragraph': '\n\n',
}

def parse_word(word):
    word = word.lstrip('\\').split('\\', 1)[0]
    word = mapping.get(word, word)
    return word

def text(m):
    tmp = [str(s).lower() for s in m.dgndictation[0]._words]
    words = [parse_word(word) for word in tmp]
    Str(' '.join(words))(None)

def word(m):
    tmp = [str(s).lower() for s in m.dgnwords[0]._words]
    words = [parse_word(word) for word in tmp]
    Str(' '.join(words))(None)

def surround(by):
    def func(i, word, last):
        if i == 0: word = by + word
        if last: word += by
        return word
    return func

def rot13(i, word, _):
    out = ''
    for c in word.lower():
        if c in string.ascii_lowercase:
            c = chr((((ord(c) - ord('a')) + 13) % 26) + ord('a'))
        out += c
    return out

formatters = {
    # 'dunder': (True,  lambda i, word, _: '__%s__' % word if i == 0 else word),  #__go__tothestore
    'camel':  (True,  lambda i, word, _: word if i == 0 else word.capitalize()), #goToTheStore
    'snake caps':  (True,  lambda i, word, _: word.upper() if i == 0 else '_'+word.upper()), #GO_TO_THE_STORE
    'snake':  (True,  lambda i, word, _: word if i == 0 else '_'+word), #go_to_the_store
    'smash':  (True,  lambda i, word, _: word), #gotothestore
    # spinal or kebab?
    'kebab':  (True,  lambda i, word, _: word if i == 0 else '-'+word), #go-to-the-store
    'title':  (False, lambda i, word, _: word.capitalize()), #Go To The Store
    'allcaps': (False, lambda i, word, _: word.upper()), #GO TO THE STORE
    'dub string': (False, surround('"')),
    'string': (False, surround("'")),
    # 'padded': (False, surround(" ")),
    # 'rot thirteen':  (False, rot13), 
}

def FormatText(m):
    fmt = []
    for w in m._words:
        if isinstance(w, Word):
            fmt.append(w.word)
    words = [str(s).lower() for s in m.dgndictation[0]._words]

    tmp = []
    spaces = True
    for i, word in enumerate(words):
        word = parse_word(word)
        for name in reversed(fmt):
            smash, func = formatters[name]
            word = func(i, word, i == len(words)-1)
            spaces = spaces and not smash
        tmp.append(word)
    words = tmp

    sep = ' '
    if not spaces:
        sep = ''
    Str(sep.join(words))(None)

ctx = Context('input')

keymap = {}
keymap.update(alpha)
keymap.update({
    'phrase <dgndictation> [over]': text,
    'say <dgndictation> [over]': text,
    'word <dgnwords>': word,
    '(%s)+ <dgndictation>' % (' | '.join(formatters)): FormatText,

    'tab':   Key('tab'),
    'left':  Key('left'),
    'right': Key('right'),
    'up':    Key('up'),
    'down':  Key('down'),

    'delete': Key('backspace'),

    'slap': [Key('cmd-right enter')],
    'enter': Key('enter'),
    'escape': Key('esc'),
    'question [mark]': '?',
    '(minus | dash)': '-',
    'plus': '+',
    'tilde': '~',
    '(bang | exclamation point)': '!',
    'dollar [sign]': '$',
    'downscore': '_',
    '(semi | semicolon)': ';',
    'colon': ':',
    '(square | left square [bracket])': '[', '(rsquare | are square | right square [bracket])': ']',
    '(paren | left paren)': '(', '(rparen | are paren | right paren)': ')',
    '(brace | left brace)': '{', '(rbrace | are brace | right brace)': '}',
    '(angle | left angle | less than)': '<', '(rangle | are angle | right angle | greater than)': '>',

    '(star | asterisk)': '*',
    '(pound | hash [sign] | octo | thorpe | number sign)': '#',
    'percent [sign]': '%',
    'caret': '^',
    'at sign': '@',
    '(and sign | ampersand | amper)': '&',
    'pipe': '|',

    '(dubquote | double quote)': '"',
    'quote': "'",
    'triple quote': "'''",
    '(dot | period)': '.',
    'comma': ', ',
    'space': ' ',
    '[forward] slash': '/',
    'backslash': '\\',

    '(dot dot | dotdot)': '..',
    'cd': 'cd ',
    'run make (durr | dear)': 'mkdir ',
    'run git': 'git ',
    'run git clone': 'git clone ',
    'run git diff': 'git diff ',
    'run git commit': 'git commit ',
    'run git push': 'git push ',
    'run git pull': 'git pull ',
    'run git status': 'git status ',
    'run git add': 'git add ',
    'run (them | vim)': 'vim ',
    'run ellis': 'ls\n',
    'dot pie': '.py',
    'run make': 'make\n',
    'run jobs': 'jobs\n',

    'const': 'const ',
    'static': 'static ',
    'tip pent': 'int ',
    'tip char': 'char ',
    # 'tip byte': 'byte ',
    # 'tip pent 64': 'int64_t ',
    # 'tip you pent 64': 'uint64_t ',
    # 'tip pent 32': 'int32_t ',
    # 'tip you pent 32': 'uint32_t ',
    # 'tip pent 16': 'int16_t ',
    # 'tip you pent 16': 'uint16_t ',
    # 'tip pent 8': 'int8_t ',
    # 'tip you pent 8': 'uint8_t ',

    'args': ['()', Key('left')],
    'index': ['[]', Key('left')],
    'block': [' {}', Key('left enter enter up tab')],
    'empty array': '[]',
    'empty dict': '{}',

    # 'state deaf': 'def ', 
    # 'state else if': 'elif ',
    'state if': 'if ',
    'state else if': [' else if ()', Key('left')],
    'state while': ['while ()', Key('left')],
    'state for': ['for ()', Key('left')],
    'state for': 'for ',
    'state switch': ['switch ()', Key('left')],
    'state case': ['case \nbreak;', Key('up')],
    'state goto': 'goto ',
    'state import': 'import ',

    # 'comment see': '// ',
    # 'comment py': '# ',

    'word queue': 'queue',
    'word eye': 'eye',
    # 'word bson': 'bson',
    # 'word iter': 'iter',
    'word no': 'NULL',
    'word cmd': 'cmd',
    # 'word dup': 'dup',
    # 'word streak': ['streq()', Key('left')],
    # 'word printf': 'printf',

    # 'word lunixbochs': 'lunixbochs',

    'dunder in it': '__init__',

    '[is] equal to': ' == ',
    'equals': '=',
    'arrow': '->',
    'call': '()',
    'indirect': '&',
    'dereference': '*',

    'new window': Key('cmd-n'),
    'next window': Key('cmd-`'),
    'last window': Key('cmd-shift-`'),
    'next app': Key('cmd-tab'),
    'last app': Key('cmd-shift-tab'),
    'next tab': Key('ctrl-tab'),
    'new tab': Key('cmd-t'),
    'last tab': Key('ctrl-shift-tab'),

    'next space': Key('cmd-alt-ctrl-right'),
    'last space': Key('cmd-alt-ctrl-left'),

    # 'scroll down': [Key('down')] * 30,
    # 'scroll up': [Key('up')] * 30,
})
ctx.keymap(keymap)
