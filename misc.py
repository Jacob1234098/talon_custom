from talon import tap

def on_key(typ, e):
    mask = tap.CTRL | tap.ALT | tap.DOWN
    if e.code == 0 and (e.flags & mask == mask):
        eye.config.control_mouse = not eye.config.control_mouse

tap.register(tap.KEY, on_key)