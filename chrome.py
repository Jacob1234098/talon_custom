from talon.voice import Context, Key

ides = ['com.google.Chrome']
ctx = Context('chrome', func=lambda app, win: any(
    i in app.bundle for i in ides))

keymap = {
    'nestab': Key('ctrl-tab'),
    'lastab': Key('ctrl-shift-tab'),
    'go back': Key('cmd-left'),
    'go forward': Key('cmd-right'),
    'run script': Key('cmd-shift-\\'),
    'link': Key('f'),
    'new link': Key('shift-f'),

    'soft load': Key('cmd-r'),
    'hard load': Key('cmd-shift-r'),

    #debug 
    'console': Key('cmd-alt-j'),
    'go File': Key('cmd-p'),
    'go function': Key('cmd-shift-o'),
    'device mode': Key('cmd-shift-m'),
    'go line': Key('ctrl-g'),
    'close dev tab': Key('alt-w'),
    'dev tools': Key('cmd-alt-i'),
    'break point': Key('cmd-b'),
    'tog points': Key('cmd-f8'),
    'runnit': Key('f8'),
    'step over': Key('ctrl-\''),
    'step into': Key('ctrl-\;'),
    'step out': Key('ctrl-shift-+'),
    'stack in': Key('ctrl-\,'),
    'stack out': Key('ctrl-\.')
}

ctx.keymap(keymap)


