from talon.voice import Context, Key

ctx = Context('editing')

keymap = {
    'sage': Key('cmd-s'),
    'dizzle': Key('cmd-z'),
    'rizzle': Key('cmd-shift-z'),
    'cutta': Key('cmd-x'),
    'copy': Key('cmd-c'),
    'slat': Key('cmd-a'),
    'pasta': Key('cmd-v'),
}

ctx.keymap(keymap)


